import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-parent',
  template: `
    <a (click)="changeFromParent()">Change from parent</a>
    <br/>
    <app-child [parentData]=data></app-child>
  `
})
export class ParentComponent implements OnInit, OnChanges {
  @Input() data = 0
  constructor() {
  }
  ngOnChanges(changes: SimpleChanges): void {
    console.log("Parent");
    console.log(changes);
  }

  ngOnInit() {
  }

  changeFromParent(){
    this.data += 1;
    console.log(("parent"));
    
  }
}