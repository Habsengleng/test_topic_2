import { Component, EventEmitter, OnChanges, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-child2',
  template: `
    <button (click)="sendToParent()">Send to parent</button>
  `,
  styles: [
  ]
})
export class Child2Component implements OnInit, OnChanges {
  @Output() messenger = new EventEmitter();
  message = "Hello Cambodia";

  sendToParent():any{
    this.messenger.emit(this.message);
  }
 
  ngOnChanges(): void{
    console.log("ngOnChange child2");
  }

  constructor() { }

  ngOnInit(): void {
  }

}
