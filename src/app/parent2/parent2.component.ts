import { Component, OnChanges, OnInit } from '@angular/core';

@Component({
  selector: 'app-parent2',
  template: `
    <p>
      parent2 works!
    </p>
    <app-child2 (messenger)="alertHello($event)"></app-child2>
  `,
  styles: [
  ]
})
export class Parent2Component implements OnInit, OnChanges {

  constructor() { }

  alertHello(message){
    alert(message);
  }

  ngOnChanges(): void{
    console.log("ngOnChange parent2");
  }

  ngOnInit(): void {
  }

}
