import { Component, ViewChild } from '@angular/core';
import {ViewchildComponent} from './viewchild/viewchild.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'untitled';
  // counter:number;
  // displayCounter(count) : any {
  //   this.counter=count;
  @ViewChild(ViewchildComponent) data:ViewchildComponent;
  shows()
  {
    this.data.show();
  }
}
